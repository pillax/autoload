<?php
namespace vendor\pillax\autoload\src;

abstract class Autoload {

    private static $basepath;

    public static function setBasepath($path) {
        self::$basepath = $path;
    }

    public static function start() : void {
        spl_autoload_register([__CLASS__, 'load']);
    }

    public static function stop() : void {
        spl_autoload_unregister([__CLASS__, 'load']);
    }

    public static function load($className) : void {
        $className = str_replace('\\', DIRECTORY_SEPARATOR, $className);
        require_once self::$basepath . $className . '.php';
    }
}